import {
  Component,
  OnInit
} from '@angular/core';
import Swal from 'sweetalert2'
import {
  HttpClient
} from '@angular/common/http';
import {
  ServiceService
} from '../app/service/service/service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  digito1: number = 0;
  digito2: number = 0;
  selOption: any = "";
  changeD1: boolean = false;
  changeD2: boolean = false;
  operador: any;
  resultado: any;

  validador: boolean = false;
  noRandom: any;
  arrRandom = [];
  arregloDeSubCadenas = [];

  constructor(private http: HttpClient, private _service: ServiceService) {}

  ngOnInit(): void {

  }

  change1(ev) {
    this.digito1 = ev.target.value;
    this.changeD1 = true;
    console.log(this.digito1);
  }

  change2(ev) {
    this.digito2 = ev.target.value;
    this.changeD2 = true;
    console.log(this.digito2);
  }

  onSelect(ev) {
    this.selOption = ev.target.value;
    console.log(this.selOption);
  }

  async onClick() {
    if (this.changeD1 === false) {
      Swal.fire(
        'Falta ingresar el digito 1',
        'Por favor, rellene todos los campos',
        'error'
      )
    } else if (this.changeD2 === false) {
      Swal.fire(
        'Falta ingresar el digito 2',
        'Por favor, rellene todos los campos',
        'error'
      )
      // return this.http.post(this.url, );
    } else if (this.selOption === "") {
      Swal.fire(
        'Falta ingresar la operacion matematica',
        'Por favor, rellene todos los campos',
        'error'
      )
    } else {
      if (this.selOption === "Suma") {
        this.operador = '%2B';
        let data = this.digito1 + this.operador + this.digito2;
        console.log(data)
        await this._service.handleOperator(data)
          .subscribe((res) => {
            this.resultado = res;
            this.randomLetter(this.resultado);
            this.validador = true;
            console.log(res);
          }, (err) => {
            console.log(err);
          });
      } else if (this.selOption === "Resta") {
        this.operador = '-';
        let data = this.digito1 + this.operador + this.digito2;
        console.log(data)
        await this._service.handleOperator(data)
          .subscribe((res) => {
            this.resultado = res;
            this.randomLetter(this.resultado);
            this.validador = true;
            console.log(res);
          }, (err) => {
            console.log(err);
          });
      } else if (this.selOption === "Multiplicacion") {
        this.operador = '*';
        let data = this.digito1 + this.operador + this.digito2;
        console.log(data)
        await this._service.handleOperator(data)
          .subscribe((res) => {
            this.resultado = res;
            this.randomLetter(this.resultado);
            this.validador = true;
            console.log(res);
          }, (err) => {
            console.log(err);
          });
      } else if (this.selOption === "Division") {
        this.operador = '%2F';
        let data = this.digito1 + this.operador + this.digito2;
        console.log(data)
        await this._service.handleOperator(data)
          .subscribe((res) => {
            this.resultado = res;
            this.randomLetter(this.resultado);
            this.validador = true;
            console.log(res);
          }, (err) => {
            console.log(err);
          });
      } else {

      }
    }

  }

  randomLetter(length) {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength)) + " ";
      console.log(result)
    }
    let separador = " "
    this.arregloDeSubCadenas = result.split(separador);
    console.log(this.arregloDeSubCadenas)

    this.noRandom = result;
    // console.log(this.noRandom);
  }

  vocal(item) {
    if (item === 'a') {
      Swal.fire(
        'Felicidades',
        'Es una vocal',
        'success'
      )
    } else if (item === 'e') {
      Swal.fire(
        'Felicidades',
        'Es una vocal',
        'success'
      )
    } else if (item === 'i') {
      Swal.fire(
        'Felicidades',
        'Es una vocal',
        'success'
      )
    } else if (item === 'o') {
      Swal.fire(
        'Felicidades',
        'Es una vocal',
        'success'
      )
    } else if (item === 'u') {
      Swal.fire(
        'Felicidades',
        'Es una vocal',
        'success'
      )
    } else {

    }
  }

  changeText(ev) {
    let result = ev.target.value
    console.log(result);
    let cambio = Array.from(result.replace(/\s/g, '')).join(" ");
    
    let separador = " "
    this.arregloDeSubCadenas = cambio.split(separador);
    console.log(this.arregloDeSubCadenas)
  }
}
