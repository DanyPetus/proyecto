import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  url = "http://api.mathjs.org/v4/?expr=";

  constructor(private http: HttpClient) { }

  httpOptions : any = {
    headers: new Headers ({
      'Content-Type': 'application/json'
    })
  };

  handleOperator(body){
    let data = body
    return this.http.get(this.url + data);
  }


}
